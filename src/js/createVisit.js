import { cardDataArr, userVisit } from "./createVisitCard.js";
import { testValidButt } from "./createVisit/validInput.js";

import { showStoredCards, saveCardsToLocalStorage } from "./LS.js";
import { checkTocken } from "./header/login.js";
import { globalTocken } from "./header/login.js";

// відправка введених даних на сервер
function sendDataToTheServer() {
  const welcome = document.querySelector(".welcome");
  const createButt = document.querySelector(".create_visit_butt");
  const visitForm = document.querySelector("form");

  const doctorsSelect = document.getElementById("doctors-select");
  const urgencySelect = document.getElementById("urgency-select");

  const searchFilter = document.querySelector(".filter");

  doctorsSelect.addEventListener("change", () => {
    const selectedDoctor =
      doctorsSelect.options[doctorsSelect.selectedIndex].value;
    createListBasedOnTheDoctor(selectedDoctor);

    testValidButt(selectedDoctor);

    clearValidationErrors();
  });

  createButt.addEventListener("click", () => {
    //перетворення отриманих даних в новий обєкт
    const formData = new FormData(visitForm);

    // додавання обраного значення з випадаючого списку до створеного обєкту formData
    const selectedDoctor =
      doctorsSelect.options[doctorsSelect.selectedIndex].value;
    formData.append("doctor", selectedDoctor);

    const selectedUrgency =
      urgencySelect.options[urgencySelect.selectedIndex].value;
    formData.append("urgency", selectedUrgency);

    if (!globalTocken) globalTocken = checkTocken();
    fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${globalTocken}`,
      },
      body: JSON.stringify(Object.fromEntries(formData)),
    })
      .then((response) => response.json())

      .then((data) => {
        //виведення карток на екран
        const cardWrapper = document.querySelector(".cards_space");
        cardDataArr.push(data);

        //оновлення картки після редагування 
        const visit = new userVisit(data.id, cardWrapper);
        visit.showCard(data);

        cardWrapper.innerHTML = "";
        cardDataArr.forEach((card) => {
          let visit = new userVisit(card.id, cardWrapper);
          visit.showCard(card);
        });

        saveCardsToLocalStorage();
        //це важливий коммент НЕ ВИДАЛЯТИ!
        // localStorage.removeItem("storedCards");

        welcome.style.display = "none";
        searchFilter.style.display = "block";
        createButt.style.display = "none";
        //скидання форми
        visitForm.reset();
        clearValidationErrors();
      })
      .catch((error) => {
        console.error("An error occurred:", error);
      });
  });
}

export function createListBasedOnTheDoctor(doctor) {
  const allDoctors = document.querySelectorAll(
    ".cardiologist, .dentist, .therapist"
  );
  allDoctors.forEach((e) => {
    // перевірка за класом
    e.classList.contains(doctor)
      ? (e.style.display = "block")
      : (e.style.display = "none");
  });
}

//прибирання попередження про валідацію
function clearValidationErrors() {
  const validTest = document.querySelectorAll(".form-control");
  validTest.forEach((input) => {
    input.classList.remove("is-invalid");
    input.classList.remove("is-valid");
  });
}

sendDataToTheServer();
showStoredCards();
