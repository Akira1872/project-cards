export function testValidButt (doctor){
    // додавання валідації
const validTest = document.querySelectorAll(`.form-control.${doctor}`);
const createButt = document.querySelector(".create_visit_butt");

validTest.forEach(input => {
    input.addEventListener('input', () => {
        let allValid = true;
        
        // перевірка на валідність всіх інпутів 
        validTest.forEach(field => {
            if (!isValid(field.value)) {
                    allValid = false;
             }
        });
            
        createButt.style.display = allValid ? 'block' : 'none';

        if (!isValid(input.value)) {
            input.classList.add('is-invalid');
            input.classList.remove('is-valid');
        } else {
            input.classList.remove('is-invalid'); 
            input.classList.add('is-valid');
        }
    });

    input.addEventListener('click', () => {
        input.classList.add('is-invalid');
    });
});
 

//перевірка на валідацію
function isValid(input) {
    return input !== null && input.trim() !== '';
}

} 