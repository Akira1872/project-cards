export class Modal {
    constructor(idModal,label,title, body) {
        this.idModal = idModal;
        this.label = label;
        this.title = title;
        this.body = body;
    }

    render () {
        const modalParent = document.querySelector('.root');
                document.body.insertAdjacentHTML('beforeend',
                // видалено блюр
                    `<div class=" fade show"></div>`);
                modalParent.insertAdjacentHTML('afterend', `
        <div class="modal fade show" id="${this.idModal}" tabindex="-1"
            aria-labelledby="${this.label}" aria-hidden="true" style="display:block">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title fs-6" id="${this.label}">${this.title}</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        ${this.body}
                    </div>
                </div>
            </div>
        </div>
        `);
    }


};


export class ModalLogin extends Modal {
    constructor (idModal = 'authorization-form-modal',label = 'authorizationModalLabel',title = 'Авторизація', email = '', password = '') {
    super( idModal, label, title )
    this.email = email;
    this.password = password;
    this.body = `
    <form>
        <div class="mb-3 mt-3">
            <label for="email" class="form-label">Email:</label>
            <input type="email" class="form-control" id="email" value="${this.email}" required
                placeholder="Введіть адресу електронної пошти" name="email">
        </div>
        <div class="mb-3">
            <label for="pwd" class="form-label">Пароль</label>
            <input type="password" class="form-control" id="pwd" value="${this.password}"  required 
                placeholder="Введіть пароль" name="pswd">
        </div>
        <div class="wrapper__invalid-message">
            <p class="invalid-message text-center text-danger mt-2 mb-0"></p>
        </div>
        <button type="submit" class="btn btn-send">Надіслати</button>
    </form>
    `;
    }

    emptyInputMessage() {
        const invalidMessage = document.querySelector('.invalid-message')
        invalidMessage.innerHTML = 'Ви ввели не всі дані!';
    }
    removeInvalidMessage() {
        const invalidMessage = document.querySelector('.invalid-message')
        invalidMessage.innerHTML = '';
    }

}
