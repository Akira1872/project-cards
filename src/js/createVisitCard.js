import { checkTocken } from "./header/login.js";
import { globalTocken } from "./header/login.js";
import { saveCardsToLocalStorage } from "./LS.js";
import { modal } from "./createVisit/buttonCreateVisit.js";
import { modalData, setModalData, getModalData } from "./header/login.js";

export const API = "https://ajax.test-danit.com/api/v2/cards";

export async function sendRequest(url, method) {
  if (!globalTocken) globalTocken = checkTocken();
  if (!globalTocken) globalTocken = checkTocken();
  const response = await fetch(url, {
    method: method,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${globalTocken}`,
    },
    // mode: "no-cors",
  });
  const result = await response.json();
  return result;
}

// масив в якому будуть клонуватися і зберігатися створені картки, щоб була можливість маніпулювати нею
export let cardDataArr = [];

// Батьківський клас
export class userVisit {
  constructor(id, cardWrapper) {
    this.id = id;
    this.card = null;
    this.cardWrapper = cardWrapper;
    this.cardData = null;
  }
  async renderData() {
    const currencyCards = await sendRequest(API);
    return currencyCards;
  }

  showCard(data) {
    this.card = document.createElement("div");
    this.card.classList.add("margin");
    // дуже тупо змінюються дані про лікаря, але так як у нас їх всього 3, то хай буде так :D
    if (data.doctor === "dentist") {
      const doctor = "Григоренко Андрій Віталійович";
      const img = "../img/dantist.jpg";
      this.card.innerHTML = `
     <div class="card" id = ${data.id}>
        <div class="card-header">
          <div class="card-header__img">
            <img class="card-img" src="${img}" alt="" />
          </div>
          <div class="card-header__info">
            <p class="card-header__info__doctor">${doctor}</p>
            <p class="card-header__info__profession " id="specialization">
              ${data.doctor}
            </p>
          </div>
        </div>

        <div class="card-footer">
          <div class="card-footer__header">
            <p class="card-footer__header__tittle">Ваш запис</p>
          </div>
          <div class="card-footer__info">
            <div class="info_keys">
              <div class="info_main">
               <div class = "info_item key">Ім'я пацієнта:     <span class="status">${data.name}</span></div>
              <p class = "info_item key">Мета:        <span class="status"> ${data.purpose}</span></p>
              <p class = "info_item key">Опис скарги:      <span class="status">${data.brief}</span></p>
              <p class = "info_item key">Терміновість:  <span class="status">${data.urgency}</span></p>
            </div>
            </div>
            <div class="info_optional cardiologist_card disabled">
             <p class = "info_item key">Останній візит:<span class="status">${data.lasteVisit}</span></p>
                            </div>
          </div>
        </div>
        <div class="card-sidebar">
          <div class="exit_logo__container">
            <img class="exit_logo" src="../img/cross.svg" alt="" />
          </div>
          <button class="card-sidebar__button open_more">Показати більше</button>
          <button class="card-sidebar__button edit">Редагувати</button>
        </div>
      </div>
      `;
    } else if (data.doctor === "cardiologist") {
      const doctor = "Андрущенко Єлена Олександрівна";
      const img = "../img/cardiolog.webp";
      this.card.innerHTML = `
     <div class="card" id = ${data.id}>
        <div class="card-header">
          <div class="card-header__img">
            <img class="card-img" src=${img} alt="" />
          </div>
          <div class="card-header__info">
            <p class="card-header__info__doctor">${doctor}</p>
            <p class="card-header__info__profession  " id="specialization">
              ${data.doctor}
            </p>
          </div>
        </div>

        <div class="card-footer">
          <div class="card-footer__header">
            <p class="card-footer__header__tittle">Ваш запис</p>
          </div>
          <div class="card-footer__info">
            <div class="info_keys">
             <div class="info_main">
               <div class = "info_item key">Ім'я пацієнта:     <span class="status">${data.name}</span></div>
              <p class = "info_item key">Мета:        <span class="status"> ${data.purpose}</span></p>
              <p class = "info_item key">Опис скарги:      <span class="status">${data.brief}</span></p>
              <p class = "info_item key">Терміновість:  <span class="status">${data.urgency}</span></p>
            </div>
            <div class="info_optional cardiologist_card disabled">
             <p class = "info_item key">Вік:<span class="status">${data.age}</span></p>
              <p class = "info_item key">Тиск:<span class="status">${data.blood}</span></p>
              <p class = "info_item key">Індекс маси тіла:<span class="status">${data.body}</span></p>
              <p class = "info_item key">Хвороби серця:<span class="status">${data.age}</span></p>
                         </div>
            </div>
          </div>
        </div>
        <div class="card-sidebar">
          <div class="exit_logo__container">
            <img class="exit_logo" src="../img/cross.svg" alt="" />
          </div>
          <button class="card-sidebar__button open_more">Показати більше</button>
          <button class="card-sidebar__button edit">Редагувати</button>
        </div>
      </div>
      `;
    } else if (data.doctor === "therapist") {
      const doctor = "Чихурська Катерина леонідівна";
      this.card.classList.add("margin");
    // дуже тупо змінюються дані про лікаря, але так як у нас їх всього 3, то хай буде так :D
    if (data.doctor === "dentist") {
      const doctor = "Григоренко Андрій Віталійович";
      const img = "../img/dantist.jpg";
      this.card.innerHTML = `
     <div class="card" id = ${data.id}>
        <div class="card-header">
          <div class="card-header__img">
            <img class="card-img" src="${img}" alt="" />
          </div>
          <div class="card-header__info">
            <p class="card-header__info__doctor">${doctor}</p>
            <p class="card-header__info__profession" id="specialization">
              ${data.doctor}
            </p>
          </div>
        </div>

        <div class="card-footer">
          <div class="card-footer__header">
            <p class="card-footer__header__tittle">Ваш запис</p>
          </div>
          <div class="card-footer__info">
            <div class="info_keys">
              <ul class="keys">
                <li class="info_item key">Ім'я пацієнта:</li>
                <li class="info_item key">Мета:</li>
                 <li class="info_item key">Опис скарги</li>
                <li class="info_item key">Терміновсть:</li>
              </ul>
            </div>
            <div class="info_status">
              <ul class="status">
             <li class="info_item status">${data.name}</li>
                <li class="info_item status">${data.purpose}</li>
                <li class="info_item status">${data.brief}</li>
                <li class="info_item status" id="regular">${data.urgency}</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="card-sidebar">
          <div class="exit_logo__container">
            <img class="exit_logo" src="../img/cross.svg" alt="" />
          </div>
          <button class="card-sidebar__button open_more">Показати більше</button>
          <button class="card-sidebar__button edit">Редагувати</button>
        </div>
      </div>
      `;
    } else if (data.doctor === "cardiologist") {
      const doctor = "Андрущенко Єлена Олександрівна";
      const img = "../img/cardiolog.webp";
      this.card.innerHTML = `
     <div class="card" id = ${data.id}>
        <div class="card-header">
          <div class="card-header__img">
            <img class="card-img" src=${img} alt="" />
          </div>
          <div class="card-header__info">
            <p class="card-header__info__doctor">${doctor}</p>
            <p class="card-header__info__profession" id="specialization">
              ${data.doctor}
            </p>
          </div>
        </div>

        <div class="card-footer">
          <div class="card-footer__header">
            <p class="card-footer__header__tittle">Ваш запис</p>
          </div>
          <div class="card-footer__info">
            <div class="info_keys">
              <ul class="keys">
                  <li class="info_item key">Ім'я пацієнта:</li>
                <li class="info_item key">Мета:</li>
                 <li class="info_item key">Опис скарги</li>
                <li class="info_item key">Терміновсть:</li>
              </ul>
            </div>
            <div class="info_status">
              <ul class="status">
              <li class="info_item status">${data.name}</li>
                <li class="info_item status">${data.purpose}</li>
                <li class="info_item status">${data.brief}</li>
                <li class="info_item status" id="regular">${data.urgency}</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="card-sidebar">
          <div class="exit_logo__container">
            <img class="exit_logo" src="../img/cross.svg" alt="" />
          </div>
          <button class="card-sidebar__button open_more">Показати більше</button>
          <button class="card-sidebar__button edit">Редагувати</button>
        </div>
      </div>
      `;
    } else if (data.doctor === "therapist") {
      const doctor = "Чихурська Катерина леонідівна";
      this.card.innerHTML = `
     <div class="card" id = ${data.id}>
        <div class="card-header">
          <div class="card-header__img">
            <img class="card-img" src="../img/pediatr.jpg" alt="" />
          </div>
          <div class="card-header__info">
            <p class="card-header__info__doctor">${doctor}</p>
            <p class="card-header__info__profession "  id="specialization">
              ${data.doctor}
            </p>
          </div>
        </div>

        <div class="card-footer">
          <div class="card-footer__header">
            <p class="card-footer__header__tittle">Ваш запис</p>
          </div>
          <div class="card-footer__info">
            <div class="info_main">
                          <div class = "info_item key">Ім'я пацієнта:     <span class="status">${data.name}</span></div>
              <p class = "info_item key">Мета:        <span class="status"> ${data.purpose}</span></p>
              <p class = "info_item key">Опис скарги:      <span class="status">${data.brief}</span></p>
              <p class = "info_item key">Терміновість:  <span class="status">${data.urgency}</span></p>
                          </div>
            <div class="info_optional therapist_card disabled " >
              <p class = "info_item key">Вік:<span class="status">${data.age}</span></p>
            </div>
          </div>
        </div>
        <div class="card-sidebar">
          <div class="exit_logo__container">
            <img class="exit_logo" src="../img/cross.svg" alt="" />
          </div>
          <button class="card-sidebar__button open_more">Показати більше</button>
          <button class="card-sidebar__button edit">Редагувати</button>
        </div>
      </div>
      `;
    }
    }
    const delButton = this.card.querySelector(".exit_logo");
    const showMore = this.card.querySelector(".open_more");
    const space = document.querySelector(".cards_space");
    const editButton = this.card.querySelector(".edit");
    space.style.display = "flex";
    space.style.flexdirection = "column-reverse";
    delButton.addEventListener("click", () => {
      this.deleteCard();
    });
    showMore.addEventListener("click", () => {
      this.showMoreInfo(data);
    });
    editButton.addEventListener("click", () => {
      this.editCard(data);
    });
    this.cardWrapper.append(this.card);
  }
  showMoreInfo(data) {
    if (data.doctor === "cardiologist") {
      const visit = new VisitCardiologist(data);
      visit.addNewKeys(data);
    } else if (data.doctor === "dentist") {
      const visit = new VisitDentist(data);
      visit.addNewKeys(data);
    } else if (data.doctor === "therapist") {
      const visit = new Visittherapist();
      visit.addNewKeys(data);
    }
  }

  editCard(data) {
    setModalData(data);
    localStorage.setItem('modalData', JSON.stringify(data));
    if (data.doctor === "cardiologist") {
      const visit = new VisitCardiologist(data);
      visit.fillModal(data);
      modal.show(visit);
      visit.updateCard(data);
      
    } else if (data.doctor === "dentist") {
      const visit = new VisitDentist(data);
      visit.fillModal(data);
      modal.show(visit);
      visit.updateCard(data);
      
    } else if (data.doctor === "therapist") {
      const visit = new Visittherapist();
      visit.fillModal(data);
      modal.show(visit);
      visit.updateCard(data);
    }
  }

  async updateCard(data) {
    if (!globalTocken) globalTocken = checkTocken();
    const updateRequest = await fetch(`https://ajax.test-danit.com/api/v2/cards/${this.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${globalTocken}`,
      },
      body: JSON.stringify(data), // Оновлені дані
    });
  
    if (updateRequest.ok) {
      console.log("Карта була успішно оновлена");
    } else {
      console.error("Не вдалося оновити карту");
    }
  }

  async deleteCard() {
    if (!globalTocken) globalTocken = checkTocken();
    if (!globalTocken) globalTocken = checkTocken();
    const delRequest = await fetch(
      `https://ajax.test-danit.com/api/v2/cards/${this.id}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${globalTocken}`,
        },
      }
    );
    if (delRequest.ok) {
      const currentIndex = cardDataArr.findIndex((el) => {
        if (el.id === this.id) {
          return true;
        }
      });
      const indexToDelete = cardDataArr.findIndex(
        (card) => card.id === this.id
      );
      if (indexToDelete !== -1) {
        cardDataArr.splice(indexToDelete, 1);
      }
      saveCardsToLocalStorage();
      cardDataArr.splice(currentIndex, 1);
      document.getElementById(this.id).remove();
    }
  }
}

class VisitCardiologist extends userVisit {
  constructor(data) {
    super(data);
  }
  addNewKeys(data) {
    const optional = document.getElementById(`${data.id}`);
    const optionaldata = optional.querySelector(".info_optional");
    optionaldata.classList.toggle("active");
  }
  fillModal(data){
    const doctorSelect = document.querySelector('#doctors-select');
    const purposeInput = document.querySelector('#purpose');
    const briefInput = document.querySelector('#brief');
    const urgencySelect = document.querySelector('#urgency-select');
    const nameInput = document.querySelector('#name');
    const bloodInput = document.querySelector('#blood');
    const bodyInput = document.querySelector('#body');
    const cardioInput = document.querySelector('#cardio');
    const ageInput = document.querySelector('#age');

   if(modalData){
    nameInput.value = data.name;
    purposeInput.value = data.purpose;
    briefInput.value = data.brief;
    urgencySelect.value = data.urgency;
    bloodInput.value = data.blood;
    bodyInput.validate = data.blood;
    cardioInput.value = data.cardio;
    ageInput.value = data.value
   }

};
}

class VisitDentist extends userVisit {
  constructor(data) {
    super(data);
  }
  addNewKeys(data) {
    const optional = document.getElementById(`${data.id}`);
    console.log(optional);
    const optionaldata = optional.querySelector(".info_optional");
    optionaldata.classList.toggle("active");
  }
  fillModal(data){
    const doctorSelect = document.querySelector('#doctors-select');
    const purposeInput = document.querySelector('#purpose');
    const briefInput = document.querySelector('#brief');
    const urgencySelect = document.querySelector('#urgency-select');
    const nameInput = document.querySelector('#name');
    const lasteVisitInput = document.querySelector('#lasteVisit');

   if(modalData){
    nameInput.value = data.name;
    purposeInput.value = data.purpose;
    briefInput.value = data.brief;
    urgencySelect.value = data.urgency;
    lasteVisitInput.value = data.lasteVisit;
   }
}
}

class Visittherapist extends userVisit {
  constructor(data) {
    super(data);
  }
  addNewKeys(data) {
    const optional = document.getElementById(`${data.id}`);
    const optionaldata = optional.querySelector(".info_optional");
    optionaldata.classList.toggle("active");
  }
  fillModal(data){
    const doctorSelect = document.querySelector('#doctors-select');
    const purposeInput = document.querySelector('#purpose');
    const briefInput = document.querySelector('#brief');
    const urgencySelect = document.querySelector('#urgency-select');
    const nameInput = document.querySelector('#name');
    const ageInput = document.querySelector('#age');

   if(modalData){
    nameInput.value = data.name;
    purposeInput.value = data.purpose;
    briefInput.value = data.brief;
    urgencySelect.value = data.urgency;
    ageInput.value = data.age;
   }
}
}


