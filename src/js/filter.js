import { cardDataArr } from "./createVisitCard.js";

function cardsFilter (){
    const searchInput = document.querySelector('.search_filter');
    const urgencyRegular = document.getElementById('filter_regular');

    const doctorsFilter = document.getElementById("filter_doctor");
    const urgencyFilter = document.getElementById("filter_urgency");

 
    searchInput.addEventListener('input', element =>{
        //отримуємо введні в пошук дані 
        const searchValue = element.target.value
        
        cardDataArr.forEach(e => {
       // достукатися до картки щоб виводити її вгору 
        const card = document.getElementById(`${e.id}`)
        
        //перевірка чи введений текст в пошук співпадає з текстом value
       
            const isVisible = 
            e.purpose.toLowerCase().includes(searchValue) || 
            e.brief.toLowerCase().includes(searchValue) || 
            e.name.toLowerCase().includes(searchValue) || 
            e.age.toString().includes(searchValue);
            card.classList.toggle('hide', !isVisible);
          
            // console.log(card);
            // console.log(isVisible);
            // console.log(searchValue);            
            
        });  
    });

    //фільтр лікар
    doctorsFilter.addEventListener('change', () => {

        const selectedDoctorFilter = doctorsFilter.options[doctorsFilter.selectedIndex].value;

        cardDataArr.forEach(e =>{
        const card = document.getElementById(`${e.id}`);
        const isDoctor = e.doctor === selectedDoctorFilter;
        card.classList.toggle('hide', !isDoctor && selectedDoctorFilter !== "-Лікар-");
        })
    })

    //фільтр терміновість 
    urgencyFilter.addEventListener('change', () => {

        const selectedUrgencyFilter = urgencyFilter.options[urgencyFilter.selectedIndex].value;

        cardDataArr.forEach(e =>{
        const card = document.getElementById(`${e.id}`);
        const isUrgency = e.urgency === selectedUrgencyFilter;
        card.classList.toggle('hide', !isUrgency && selectedUrgencyFilter !== "-Терміновість-");
        })
    })
   
    }


cardsFilter();
