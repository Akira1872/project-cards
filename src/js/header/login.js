import { ModalLogin } from "../modules/Module.js";
import { getToken } from "./getToken.js";

export const addIdModal = (idModal) => {
    let modalForm = document.querySelector(`${idModal}`);
    modalForm.style.display = 'block';
};


// глобалка для збереження токену 
export let globalTocken;

//глобалка для даних в модалці 
export let modalData

export function setModalData(data) {
    modalData = data;
}
  
export function getModalData() {
    return modalData;
}

export function LOGIN() {
    const btnEnter = document.querySelector('.btn-enter');
    const btnLogout = document.querySelector('.btn-logout');
    const noAuthorization = document.querySelector('.no-authorization')
    const succesBox = document.querySelector ('#btnSucces')
    const logBtn = document.querySelector ('#btnLog')

    function showSomeDetails() {
        const createVisitBtn = document.querySelector('.btn-create-visit');
        const filters = document.querySelector('.wrapper-filtering-form');
        const noCardsText = document.querySelector('.no-cards');
    

        btnEnter.classList.add('disappear');
        noAuthorization.classList.add('disappear');
        btnLogout.classList.remove('disappear');
        createVisitBtn.classList.remove('disappear');            
        filters.classList.remove('disappear');
        noCardsText.classList.remove('disappear');
    }

    if (!sessionStorage.getItem('token')) {



        btnEnter.addEventListener('click', () => {
            if (!document.querySelector('#authorization-form-modal')) {
                const form = new ModalLogin();
                form.render();
    
                const modalForm = document.querySelector('#authorization-form-modal');
                const emailForm = modalForm.querySelector('input[type="email"]');
                const passwordForm = modalForm.querySelector('input[type="password"]');
                const btnForm = modalForm.querySelector('.btn-send');
    
                btnForm.addEventListener('click', (e) => {
                    e.preventDefault();
                    if (emailForm.value && passwordForm.value) {
                        submitForm(modalForm, emailForm.value, passwordForm.value);
                        form.removeInvalidMessage();
                    } else {
                        form.emptyInputMessage();
                    }
                });
    
                modalForm.addEventListener('click', (e) => {
                    
                    if (e.target.classList.contains('modal') || e.target.classList.contains('btn-close')) {
                        modalForm.style.display = 'none';

                        emailForm.value = '';
                        passwordForm.value = '';
                        form.removeInvalidMessage();
                    }
                });


            } else {
                const modalForm = document.querySelector('#authorization-form-modal');
                modalForm.style.display = 'block';
            }
        });

    
        async function submitForm(form, email, password) {
            const token = await getToken(email, password);
            localStorage.setItem ('token', token);
    
            if (token) {
                showSomeDetails();
                form.style.display = 'none';

                //виклик для відображення інф-ї в залежності від користувача
                const getStoredArr = localStorage.getItem('storedArr');
                const showStoredArr = JSON.parse(getStoredArr); 

                //для модалки
                const storedData = localStorage.getItem('modalData');
                if (storedData) modalData = JSON.parse(storedData);  
            }
            checkTocken ()
        }

    } else {
        showSomeDetails();
    }
    
    btnLogout.addEventListener('click', () => {
        localStorage.removeItem('token')
        checkTocken()
    })

}

export function checkTocken() {
    const tocken = localStorage.getItem ('token');
    globalTocken = tocken;

    const succesBox = document.querySelector ('#btnSucces')
    const logBtn = document.querySelector ('#btnLog')

      //приховування карток
      const cardWrapper = document.querySelector(".cards_space");
      const filter = document.querySelector('.filter');
      
    if (tocken) {
        succesBox.style.display = 'block';
        logBtn.style.display = 'none';
        cardWrapper.style.display = "block";
        filter.style.display = "block";
    } else {
        succesBox.style.display = 'none';
        logBtn.style.display = 'block';
        cardWrapper.style.display = "none";
        filter.style.display = "none";
    }

}
