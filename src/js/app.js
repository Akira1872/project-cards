import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

import Swiper, { Navigation, Pagination } from "swiper";
const swiper = new Swiper();

import { LOGIN, checkTocken } from "./header/login.js";
LOGIN();
checkTocken();

import script from "./createVisit.js";

import code from "./createVisitCard.js";

// як і зі стилями, пропоную просто тут підключати наші файли

// import script from "./script.js"; це для прикладу

// як казав мені колись Роман: "На кожне завдання свій окремий js файл"
import buttCreate from "./createVisit/buttonCreateVisit.js";

import valid from "./createVisit/validInput.js";

import filter from "./filter.js";

import LS from './LS.js';