const tokenAPI = 'https://ajax.test-danit.com/api/v2/cards/login';

export async function getToken(email, password) {
    const response = await fetch(tokenAPI, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email,
            password: password,
        }),
    });

    if (response.ok) {
        return response.text();
    } else {
        alert('Користувача з такими даними не зареєстровано!');
    }
}