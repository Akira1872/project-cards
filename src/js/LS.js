import { cardDataArr, userVisit } from "./createVisitCard.js";

//створення ключа та зберігання в ньому даних
export function saveCardsToLocalStorage() {
  localStorage.setItem('storedCards', JSON.stringify(cardDataArr));
}

// витягування збережених даних зі сховища 
export function showStoredCards() {
    const cardWrapper = document.querySelector(".cards_space");
    const getStoredArr = localStorage.getItem('storedCards');
    
    if (getStoredArr) {
      const storedArr = JSON.parse(getStoredArr);
      
      // Очистити глобальний масив карток
      cardDataArr.length = 0;
  
      storedArr.forEach((card) => {
        cardDataArr.push(card);
      });
  
      cardWrapper.innerHTML = "";
      cardDataArr.forEach((card) => {
        let visit = new userVisit(card.id, cardWrapper);
        visit.showCard(card);
      });
  
      const welcome = document.querySelector(".welcome");
      const searchFilter = document.querySelector(".filter");
      const createButt = document.querySelector(".create_visit_butt");
  
      welcome.style.display = "none";
      searchFilter.style.display = "block";
      createButt.style.display = "none";
    }
  }


//перевірка на наявність ключа 
//   if (localStorage.getItem('storedCards') !== null) {
//     console.log('Ключ існує в локальному сховищі.');
// } else {
//     console.log('Ключ не існує в локальному сховищі.');
// }

